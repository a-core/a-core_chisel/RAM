// SPDX-License-Identifier: Apache-2.0

// RAM implementation with synchronous read and synchronous write
// This module can be synthesized as FPGA block RAMs
// Inititally written by Verneri Hirvonen (verneri.hirvonen@aalto.fi), 2021-07-02
package ram

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.iotesters.PeekPokeTester
import a_core_common._
import scala.math.{pow, log, round}

/** IO definitions for RAM */
class RAMIO extends Bundle {
  override def cloneType = (new RAMIO).asInstanceOf[this.type]
}

/** Module definition for RAM
  * @param ram_start Starting address of the memory expressed as chisel hex string.
    Defaults to `"h2000_00000"`.
  * @param ram_depth The width of the address bus for this memory element.
    E.g. the value 10 would correspond to 2**10 = 1KiB of memory.
  */
class RAM(XLEN: Int = 32, ram_start: String = "h2000_0000", ram_depth: Int = 14)
  extends MultiIOModule with BusDevice {

  val io = IO(new RAMIO)
  val bus_io = IO(new BusDeviceIO(XLEN=XLEN))

  def log2(i: Integer) : Integer = round(log(i.toDouble)/log(2)).toInt
  val xlen_bytes = XLEN/8           // number of bytes in a register
  val xlen_depth = log2(xlen_bytes) // how many address bits are required to index byte-wide memory blocks
  val block_depth = ram_depth - xlen_depth

  // 32-bit words divided into four separately addressable 8-bit wide memory blocks
  val ram0 = SyncReadMem(scala.math.pow(2, block_depth).intValue, UInt(8.W))
  val ram1 = SyncReadMem(scala.math.pow(2, block_depth).intValue, UInt(8.W))
  val ram2 = SyncReadMem(scala.math.pow(2, block_depth).intValue, UInt(8.W))
  val ram3 = SyncReadMem(scala.math.pow(2, block_depth).intValue, UInt(8.W))

  // offset ram_start
  val addr_local = bus_io.bulk.addr - ram_start.U
  val addr_base = addr_local & ~("b11".U(XLEN.W))
  val addr_offset = addr_local - addr_base

  // address within each memory block
  val block_addr = addr_base(ram_depth, 2)

  //======================================== Read Logic ========================================
  // memory block read ports
  val block0_rdata = WireDefault(0.U(8.W))
  val block1_rdata = WireDefault(0.U(8.W))
  val block2_rdata = WireDefault(0.U(8.W))
  val block3_rdata = WireDefault(0.U(8.W))
  when (bus_io.bulk.ren) {
    block0_rdata := ram0.read(block_addr)
    block1_rdata := ram1.read(block_addr)
    block2_rdata := ram2.read(block_addr)
    block3_rdata := ram3.read(block_addr)
  }

  // TODO: add exception logic for naturally unaligned accesses
  val loaded_byte = Mux(addr_offset === 0.U, block0_rdata,
                    Mux(addr_offset === 1.U, block1_rdata,
                    Mux(addr_offset === 2.U, block2_rdata,
                                             block3_rdata))) // addr_offset === 3.U

  val loaded_half = Mux(addr_offset === 0.U, Cat(block1_rdata, block0_rdata),
                                             Cat(block3_rdata, block2_rdata))

  val loaded_word = Cat(block3_rdata, block2_rdata, block1_rdata, block0_rdata)

  // read
  bus_io.bulk.rdata := 0.U
  when (bus_io.sel && bus_io.bulk.ren) {
    when (bus_io.bulk.op_width === MemOpWidth.BYTE) {
      bus_io.bulk.rdata := loaded_byte
    } .elsewhen (bus_io.bulk.op_width === MemOpWidth.HWORD) {
      // sign extend if op_unsigned is deasserted
      bus_io.bulk.rdata := loaded_half
    } .elsewhen (bus_io.bulk.op_width === MemOpWidth.WORD) {
      bus_io.bulk.rdata := loaded_word
    }
  }

  //======================================= Write Logic ========================================
  // memory block read ports
  // memory block write ports
  // input data muxes for memory blocks (only supports naturally aligned memory accesses)
  val block0_wdata = bus_io.bulk.wdata(7,0)
  val block1_wdata =  Mux(addr_offset === 0.U, bus_io.bulk.wdata(15,8),
                                               bus_io.bulk.wdata(7,0))  // addr_offset === 1.U
  val block2_wdata =  Mux(addr_offset === 0.U, bus_io.bulk.wdata(23,16),
                                               bus_io.bulk.wdata(7,0))  // addr_offset === 2.U
  val block3_wdata =  Mux(addr_offset === 0.U, bus_io.bulk.wdata(31,24),
                      Mux(addr_offset === 2.U, bus_io.bulk.wdata(15,8),
                                               bus_io.bulk.wdata(7,0))) // addr_offset === 3.U

  // block write enable signals (depends on addr_offset and op_width)
  val block0_en = WireDefault(false.B)
  val block1_en = WireDefault(false.B)
  val block2_en = WireDefault(false.B)
  val block3_en = WireDefault(false.B)
  when (addr_offset === 0.U) {
    block0_en := true.B
    when (bus_io.bulk.op_width === MemOpWidth.HWORD) {
      block1_en := true.B
    }
    when (bus_io.bulk.op_width === MemOpWidth.WORD) {
      block1_en := true.B
      block2_en := true.B
      block3_en := true.B
    }
  } .elsewhen (addr_offset === 1.U) {
    block1_en := true.B
  } .elsewhen (addr_offset === 2.U) {
    block2_en := true.B
    when (bus_io.bulk.op_width === MemOpWidth.HWORD) {
      block3_en := true.B
    }
  } .elsewhen (addr_offset === 3.U) {
    block3_en := true.B
  }

  when (bus_io.sel && bus_io.bulk.wen) {
    when (block0_en) { ram0.write(block_addr, block0_wdata) }
    when (block1_en) { ram1.write(block_addr, block1_wdata) }
    when (block2_en) { ram2.write(block_addr, block2_wdata) }
    when (block3_en) { ram3.write(block_addr, block3_wdata) }
  }
}

/** This gives you verilog */
object RAM extends App {
  chisel3.Driver.execute(args, () => new RAM)
}

/** This is a simple unit tester for demonstration purposes */
class UnitTester(c: RAM) extends PeekPokeTester(c) {
  // Tests are here 
  /*
  poke(c.io.A(0).real, 5)
  poke(c.io.A(0).imag, 102)
  step(5)
  fixTolLSBs.withValue(1) {
    expect(c.io.B(0).real, 5)
    expect(c.io.B(0).imag, 102)
  }
  */
}

/** Unit test driver */
object UnitTestDriver extends App {
  iotesters.Driver.execute(args, () => new RAM) {
    c => new UnitTester(c)
  }
}
